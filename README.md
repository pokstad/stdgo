# stdgo

Standard Go: The code style of the Go standard library

Contained within is a collection of observed patterns in the Go standard library
(stdlib) and other code bases maintained by the Go team.

## Why

The Go stdlib is maintained by the creators of Go. It therefore is considered by
many to be the best source of proper Go usage. Documenting these examples gives
Go developers a precedent for helping to make style decisions. It also provides
inspiration for developers who have not had the privledge of combing through the
stdlib codebase.

## Exceptions

The Go stdlib is not perfect. Over the years there has been an accumlation of
cruft due to various reasons (e.g. the Go v1 compatibility promise). There are
also mistakes and quality issues in the implementation code. Any pattern found
in the stdlib should be evaluated independently prior to inclusion.